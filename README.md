#Cloud Service Programming Exercise – Reverse Geocoding


Design and implement a RESTful web service (HTTP) which is capable of looking up a physical street address given a set of geographic coordinates (lng and lat values). For example, given the lat '33.969601' and lng '-84.100033', the service should return the address of the NCR office in Duluth, GA (2651 Satellite Blvd, Duluth, GA 30096).  The implementation should delegate to an online geocoding API (i.e., Google Maps or similar) to perform the lookup; the implementation will serve as a basic abstraction to simplify usage of one or more external services that handle the geo-location aspects.
Functional Requirements:

- The service should provide a resource for accepting lat and lng coordinates for a location on Earth.
- The service, for a valid set of coordinates, return the full, street address (including city, state/province, and zip/postal code) of the location at those coordinates.
- The service should cache (locally) the last 10 lookups and provide an additional RESTful API for retrieving this stored data.  The data returned from this API should be a collection of the lookups performed, including the lng and lat values, the address found, and the date/time of the lookup.

Non-Functional Requirements:

- The sample project must include instructions for building and running the project.  Preferably, projects should use a build tool such as Maven, Gradle, Make, etc.
- The service should handle any error conditions (such as invalid input or internal errors) with suitable HTTP error responses.
- The developer is responsible for designing the API signatures, including the input/output data structures, and any exceptions deemed necessary.
- Although Java is preferred, the choice of language and frameworks is at the discretion of the developer.  Ideally, the application will run as a simple process/executable, and not require an external container or web server to run.

Projects can be submitted to us either via a zip/tarball containing all source, or alternatively, a link to an available GitHub, Bitbucket, or similar repository.


##Prerequisites
 - Computer with Java > 1.7
 - Maven 3.3+


##Run server

Run Maven `spring-boot:run`

```bash
mvn spring-boot:run

or

java -jar target/reversegeocoder-1.0-SNAPSHOT.jar

```

##Access GUI interface

Open a browser


**Request address based on latitude and longitude example:**

```
http://localhost:8080/address?lat=33.969601&lon=-84.100033
```

**Result**:
```
2651 Satellite Blvd, Duluth, GA 30096, USA
```
**Request lookup search from cache example:**

```
http://localhost:8080/lookups
```

**Result**:

```
[ {
  "lat" : 37.969601,
  "lng" : -85.100033,
  "address" : "1520-1526 Ashby Rd, Lawrenceburg, KY 40342, USA",
  "lookupTime" : 1482173923201
}, {
  "lat" : 37.969601,
  "lng" : -84.100033,
  "address" : "Beechwood Ln, Winchester, KY 40391, USA",
  "lookupTime" : 1482173932186
}, {
  "lat" : 30.969601,
  "lng" : -84.100033,
  "address" : "5092 Maddox Rd, Ochlocknee, GA 31773, USA",
  "lookupTime" : 1482173937396
}, {
  "lat" : 30.969601,
  "lng" : -86.100033,
  "address" : "1790 Braxton Rd, Westville, FL 32464, USA",
  "lookupTime" : 1482173944122
} ]

```

##Invalid Requests Examples

**Request with non-numeric longitude value**

```
http://localhost:8080/address?lat=37.969601&lon=wueueue
```

**Result**:

```
invalid input make sure latitude and longitude values are numbers
```

**Request with missing longitude**

```
http://localhost:8080/address?lat=37.969601&lon=
```

**Result**:

```
invalid input make sure latitude and longitude values are numbers
```

**Request with longitude out of range (-180 to 180)**
```
http://localhost:8080/address?lat=37.969601&lon=-200.100033
```
**Result**:

```
invalid input latitude and longitude is not in range
```


**Request with longitude out of range (-90 to 90)**
```
http://localhost:8080/address?lat=&lon=-200.100033
```

**Result**:
```
invalid input latitude and longitude is not in range
```


**Request where a location was not found**
```
http://localhost:8080/address?lat=30.969601&lon=-80.100033
```

**Result**:
```
no location was found
```









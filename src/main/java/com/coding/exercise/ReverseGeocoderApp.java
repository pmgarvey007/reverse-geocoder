package com.coding.exercise;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by IntelliJ IDEA.
 * Developer: Paul Garvey
 * Datetime: 12/19/16 12:24 AM.
 */
@EnableAutoConfiguration
@ComponentScan
public class ReverseGeocoderApp {
    public static void main(String[] args) {
        SpringApplication.run(ReverseGeocoderApp.class, args);
    }

    /**
     * creates an pretty printing object mapper
     *
     * @return
     */
    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        return mapper;
    }
}

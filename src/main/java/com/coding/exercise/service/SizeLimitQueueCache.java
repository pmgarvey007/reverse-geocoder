package com.coding.exercise.service;

import java.util.LinkedList;

/**
 * Created by IntelliJ IDEA.
 * Developer: 221011704
 * Datetime: 12/19/16 12:58 AM.
 */
public class SizeLimitQueueCache<E> extends LinkedList<E> {
    private int limit;

    public SizeLimitQueueCache(int limit) {
        this.limit = limit;
    }

    @Override
    public boolean add(E o) {
        boolean added = super.add(o);
        while (added && size() > limit) {
            super.remove();
        }
        return true;
    }
}

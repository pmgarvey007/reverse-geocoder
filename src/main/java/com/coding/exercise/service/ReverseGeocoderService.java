package com.coding.exercise.service;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;


/**
 * Created by IntelliJ IDEA.
 * Developer: Paul Garvey
 * Datetime: 12/19/16 12:40 AM.
 */

@Service
public class ReverseGeocoderService {
    private final int CACHE_SIZE_LIMIT = 10;
    private final Logger log  = LoggerFactory.getLogger(getClass());
    private final SizeLimitQueueCache<LookupInfo> cache = new SizeLimitQueueCache<>(CACHE_SIZE_LIMIT);

    /**
     * Service that delegate to Google Map Api web service to lookup an address based on a provided latitude and longitude
     * @param lat
     * @param lng
     * @return
     * @throws LocationException
     */
    public String getAddress( double lat, double lng) throws LocationException {
        //call remote service and extract formatted address
        String formattedLocation = extractFormattedAddress(getLocationDetails(lat, lng));
        //save lookup details to cache with limit of CACHE_SIZE_LIMIT
        cacheLookup(lat, lng, formattedLocation);
        return formattedLocation;
    }


    /**
     * Get the latest lookup from the memory cache
     * @return
     */
    public List<LookupInfo> getLatestLookups() {
        return cache.subList(0, cache.size());
    }


    /**
     * method to extract the formatted address from a json object
     * @param jsonObject
     * @return
     * @throws LocationException
     */
    private String extractFormattedAddress(JSONObject jsonObject) throws LocationException {
        String formattedLocation;
        try {
            //Get JSON Array called "results" and then get the 0th complete object as JSON
            JSONArray results = jsonObject.getJSONArray("results");
            if (results.length() == 0)
                return "no location was found";
            JSONObject location = results.getJSONObject(0);
            // Get the value of the attribute whose name is "formatted_string"
            formattedLocation = location.getString("formatted_address");
        } catch (JSONException e1) {
            e1.printStackTrace();
            throw new LocationException("error parsing json");
        }
        return formattedLocation;
    }


    /**
     * method to make a remote call to Google Map API web service to get location details based on latitude and longitude
     * @param lat
     * @param lng
     * @return
     * @throws LocationException
     */
    private JSONObject getLocationDetails(double lat, double lng) throws LocationException {
        String json;
        String serviceUrl = String.format("http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true", lat, lng);
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpGet request = new HttpGet(serviceUrl);
            HttpResponse closeableHttpResponse = httpClient.execute(request);
            String statusResponseLine = closeableHttpResponse.getStatusLine().toString();
            int statusCode = closeableHttpResponse.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                log.error(String.format("Exception occurred processing response code=%s message=%s", statusCode, statusResponseLine));
                throw new LocationException("Failed with http code : " + statusCode);
            }
            // expect a valid user info json back.
            json = EntityUtils.toString(closeableHttpResponse.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
            throw new LocationException(e);
        }
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
            throw new LocationException("error parsing json");
        }
        return jsonObject;
    }

    /**
     * method to cache a lookup
     * @param lat
     * @param lng
     * @param formattedLocation
     */
    private void cacheLookup(double lat, double lng, String formattedLocation) {
        LookupInfo ls = new LookupInfo(lat, lng, formattedLocation);
        if(!cache.contains(ls)) {
            cache.add(ls);
        }
    }

}

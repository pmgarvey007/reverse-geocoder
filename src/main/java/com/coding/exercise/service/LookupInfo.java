package com.coding.exercise.service;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Developer: Paul Garvey
 * Datetime: 12/19/16 12:14 AM.
 */
public class LookupInfo implements Comparable<LookupInfo> {
    private double lat;
    private double lng;
    private String address;
    private Date lookupTime;

    public LookupInfo(double lat, double lng, String address) {
        this.lat = lat;
        this.lng = lng;
        this.address = address;
        this.setLookupTime(new Date());
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getLookupTime() {
        return lookupTime;
    }

    public void setLookupTime(Date lookupTime) {
        this.lookupTime = lookupTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof LookupInfo)) return false;

        LookupInfo that = (LookupInfo) o;

        return new EqualsBuilder()
                .append(getLat(), that.getLat())
                .append(getLng(), that.getLng())
                .append(getAddress(), that.getAddress())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getLat())
                .append(getLng())
                .append(getAddress())
                .toHashCode();
    }

    @Override
    public int compareTo(LookupInfo o) {
        int i = -1;

        if (null != address && null != o.getAddress()) {
            if (0 != (i = address.compareTo(o.getAddress()))) {
                return i;
            }
        }
        return i;
    }

    @Override
    public String toString() {
        return String.format("{lat : %f, lng : %f, address: \"%s\", lookupTime: \"%s\"", lat, lng, address, lookupTime);
    }
}

package com.coding.exercise.service;

/**
 * Created by IntelliJ IDEA.
 * Developer: Paul Garvey
 * Datetime: 12/19/16 12:30 AM.
 */
public class LocationException extends Exception {
    private static final long serialVersionUID = 5557398145333975660L;
    public LocationException(final String message) { super(message); }
    public LocationException(final Throwable t) { super(t); }
    public LocationException(final String message, final Throwable t) { super(message, t); }
}

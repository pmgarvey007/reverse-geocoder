
package com.coding.exercise.controller;

import com.coding.exercise.service.LocationException;
import com.coding.exercise.service.LookupInfo;
import com.coding.exercise.service.ReverseGeocoderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * Created by IntelliJ IDEA.
 * Developer: Paul Garvey
 * Datetime: 12/19/16 12:01 AM.
 */
@RestController
public class ReverseGeocoderController {

    @Autowired
    private ReverseGeocoderService reverseGeocoderService;

    @RequestMapping("/address")
    public ResponseEntity<?> getAddress(
            @RequestParam(value = "lat") String lat,
            @RequestParam(value = "lon") String lng) {

        try {
            //parse values to double
            double latitude = Double.parseDouble(lat);
            double longitude = Double.parseDouble(lng);
            //make sure the values are within range
            if(!inRange(latitude, longitude)) {
                return new ResponseEntity<>("invalid input latitude and longitude is not in range", HttpStatus.BAD_REQUEST);
            }
            //call service layer
            String formattedAddress = reverseGeocoderService.getAddress(latitude, longitude);
            return new ResponseEntity<>(formattedAddress, HttpStatus.OK);

        }catch (LocationException le) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }catch (NumberFormatException nfe) {
            return new ResponseEntity<>("invalid input make sure latitude and longitude values are numbers", HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping("/lookups")
    public ResponseEntity<?> getLookups() {
        List<LookupInfo>  lookups = reverseGeocoderService.getLatestLookups();
        if (null == lookups || lookups.size() == 0) {
            return new ResponseEntity<>("lookup list is empty", HttpStatus.OK);
        }
        return new ResponseEntity<>(lookups, HttpStatus.OK);
    }

    //method to make use the input values for latitude and longitude is valid (within range)
    private static boolean inRange(double lat, double lon) {
        if (Double.compare(lat, 90) <= 0 && Double.compare(lat, -90) >= 0 && Double.compare(lon, 180) <= 0 && Double.compare(lon, -180) >= 0) {
            return true;
        }
        return false;
    }

}

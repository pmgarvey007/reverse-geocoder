package com.coding.exercise.service;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by IntelliJ IDEA.
 * Developer: 221011704
 * Datetime: 12/19/16 1:10 AM.
 */
public class SizeLimitQueueCacheTest {
    private static final Logger log = LoggerFactory.getLogger(SizeLimitQueueCacheTest.class);

    private static int CACHE_SIZE_LIMIT = 10;

    @Test
    public void testAdd() {
        SizeLimitQueueCache<LookupInfo> cache = new SizeLimitQueueCache<>(CACHE_SIZE_LIMIT);
        for(int i = 0; i < CACHE_SIZE_LIMIT; i++) {
            cache.add(new LookupInfo(i+1, (i+1)*2, String.format("i - some formatted address - %d", i+1)));
        }
        assertEquals(CACHE_SIZE_LIMIT, cache.size());

        log.debug("\n\ncache after adding 10 items:\n\n");
        for(LookupInfo ls : cache) {
            log.debug("{}", ls);
        }


        for(int j = 0; j < 5; j++) {
            cache.add(new LookupInfo(j+1, (j+1)*2, String.format("j - some formatted address - %d", j+1)));
        }

        //cache size should
        assertNotEquals(15, cache.size());
        assertEquals(CACHE_SIZE_LIMIT, cache.size());

        log.debug("\n\ncache after adding additional 5 items:\n\n");
        for(LookupInfo ls : cache) {
            log.debug("{}", ls);
        }

    }
}
